import React, { useEffect, useState, ChangeEvent, FormEvent } from 'react';
import './styles.css';
import { Link, useHistory } from 'react-router-dom';
import { FiArrowLeft } from 'react-icons/fi';
import { Map, TileLayer, Marker } from 'react-leaflet';
import logo from "../../assets/logo.svg";
import { LeafletMouseEvent } from 'leaflet';
import api from '../../services/api';

interface Item {
    id: Number,
    name: string,
    image: string
}

interface IBGEUfInitial {
    sigla: string
}

interface Municipio {
    nome: string
}

const CreatePoint = () => {
    const [items, setItems] = useState<Item[]>([]); 
    const [ufs, setUfs] = useState<string[]>([]); 
    const [municipios, setMunicipios] = useState<string[]>([]);

    const [initialPosition, setInitialPosition] = useState<[number, number]>([0,0]);

    const [selectedCity, setSelectedCity] = useState<String>();
    const [selectedPosition, setSelectedPosition] = useState<[number, number]>([0,0]);
    const [selectedUF, setSelectedUF] = useState<string>("");
    const [selectedItems, setSelectedItems] = useState<Number[]>([]);

    const [formData, setFormData] = useState({
        name: '',
        email: '',
        whatsapp: '',    
    });



    useEffect(() => {
        api.get('items').then(response => {
            setItems(response.data);
        })
    }, []);

    useEffect(() => {
        api.get<IBGEUfInitial[]>('https://servicodados.ibge.gov.br/api/v1/localidades/estados').then(response => {
            const ufInitials = response.data.map(uf => uf.sigla)
            setUfs(ufInitials);
        })
    }, [])

    useEffect(() => {
        if(selectedUF === "0"){
            return;
        }

        api.get<Municipio[]>(`https://servicodados.ibge.gov.br/api/v1/localidades/estados/${selectedUF}/municipios`).then(response => {
            const municipioNames = response.data.map(mun => mun.nome);
            setMunicipios(municipioNames);
        })
    }, [selectedUF])

    useEffect(() => {
        navigator.geolocation.getCurrentPosition(position => {
            const { latitude, longitude } = position.coords;

            setInitialPosition([
                latitude,
                longitude
            ])
        });
    }, [])

    function handleUF(event: ChangeEvent<HTMLSelectElement>) {
        setSelectedUF(event.target.value);
    }

    function handleSelectedCity(event: ChangeEvent<HTMLSelectElement>){
        setSelectedCity(event.target.value);
    }
    
    function handleMapClick(event: LeafletMouseEvent){
        setSelectedPosition([
            event.latlng.lat,
            event.latlng.lng
        ]);
    }

    function handleInputChange(event: ChangeEvent<HTMLInputElement>){
        const { name, value } = event.target;

        setFormData({...formData, [name]: value})
    }

    function handleSelectedItem(id: Number){
        const alredySelected = selectedItems.findIndex(item => item === id);
        
        if(alredySelected >= 0){
            const filteredItems = selectedItems.filter(item => item !== id);

            setSelectedItems(filteredItems);
        }else{
            setSelectedItems([...selectedItems, id]);
        }
    }

    const history = useHistory();

    async function handleSubmit(event: FormEvent){
        event.preventDefault();

        const {  name, email, whatsapp } = formData;
        const uf = selectedUF;
        const city = selectedCity;
        const [ latitude, longitude ] = selectedPosition;
        const items = selectedItems;

        const data = {
            name,
            email,
            whatsapp, 
            formData,
            uf,
             selectedUF,
            city,
             selectedCity,
            latitude,
            longitude,  
            selectedPosition,
            items,
            selectedItems
        }

        await api.post('/points', data);

        history.push("/");
    }

    return (
        <div id="page-create-point">
            <header>
                <img src={logo} alt="Ecoleta"/>
                
                <Link to="/">
                    <FiArrowLeft/>

                    Voltar para Home
                </Link>
            </header>

            <form onSubmit={handleSubmit}>
                <h1>Cadastro do <br/> ponto de coleta</h1>

                <fieldset>
                    <legend>
                        <h2>Dados</h2>
                    </legend>

                    <div className="field">
                        <label htmlFor="name">Nome da entidade</label>
                        <input 
                            type="text"
                            name="name"
                            id="name"
                            onChange={handleInputChange}/>
                    </div>
                    <div className="field-group">
                    <div className="field">
                        <label htmlFor="email">E-mail</label>
                        <input 
                            type="email"
                            name="email"
                            id="email"
                            onChange={handleInputChange}/>
                    </div>
                    <div className="field">
                        <label htmlFor="whatsapp">Whatsapp</label>
                        <input 
                            type="text"
                            name="whatsapp"
                            id="whatsapp"
                            onChange={handleInputChange}/>
                    </div>
                    </div>
                </fieldset>
                <fieldset>
                    <legend>
                        <h2>Endereço</h2>
                        <span>Selecione o endereço no mapa</span>
                    </legend>
                    <Map center={[-8.1321688, -34.961686]} zoom={15} onclick={handleMapClick}>
                    <TileLayer
                        attribution='&amp;copy <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
                        url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
                        />

                        <Marker position={selectedPosition}/>
                    </Map>

                    <div className="field-group">
                        <div className="field">
                            <label htmlFor="uf">Estado (UF)</label>
                            <select name="uf" id="uf" onChange={handleUF}>
                            <option value="0">Selecione uma UF</option>
                             { ufs.map(uf=> (
                                 <option key={uf} value={uf}>{uf}</option>
                             ))}
                            </select>
                        </div>
                        <div className="field">
                            <label htmlFor="city">Cidade</label>
                            <select 
                                name="city" 
                                id="city" 
                                value={selectedUF}
                                onChange={handleSelectedCity}>
                                <option value="0">Selecione uma cidade</option>
                                { municipios.map(municipio => (
                                    <option value={municipio}>{ municipio }</option>
                                ))}
                            </select>
                        </div>
                    </div>
                </fieldset>

                <fieldset>
                    <legend>Ítens de coleta</legend>
                    <span>Selecione um ou mais ítens abaixo</span>

                    <ul className="items-grid">
                        {items.map(item => (
                            <li 
                            key={String(item.id)}
                            onClick={() => handleSelectedItem(item.id)}
                            className={selectedItems.includes(item.id) ? 'selected' : ''}
                            >
                            <img src={item.image} alt={item.name}/>
                            <span>{item.name}</span>
                        </li>
                        ))}
                    </ul>
                </fieldset>
                
                <button type="submit">
                    Cadastrar ponto de coleta
                </button>
            </form>

            
        </div>
    );
}

export default CreatePoint;