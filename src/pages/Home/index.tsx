import React from 'react';
import './styles.css';
import { FiLogIn } from 'react-icons/fi';
import logo from '../../assets/logo.svg';
import { Link } from 'react-router-dom';

const Home = () => {
    return(
        <div id="page-home">
            <div className="content">
                <header>
                <img src={logo}/>
                </header>
                <main>
                    <h1>Seu marketplace de coleta de resíduos.</h1>
                    <p>Ajudamos pessoas a encontrarem pontos de coleta de forma mais eficiente.</p>
                    
                    <Link to="/create-point">
                        <span><FiLogIn/></span>
                        <strong>Pesquisar pontos de coleta</strong>
                    </Link>
                </main>
            </div>
        </div>
    );
}

export default Home;